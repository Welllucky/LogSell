def verifnum(shownum,shownum_error='\033[1;31mValor inválido. \033[1;33mPor favor digite um número válido:\033[m '):
    while True:
        value = input(shownum).lower().lstrip().rstrip()
        if value.isnumeric() == True:
            return int(value)
            break
        print(shownum_error)

def verifname(shown,shown_error = '\033[1;33mPor favor digite um nome válido:\033[m '):
    while True:
        value = input(shown).lower().lstrip().rstrip()
        if value.isalpha() ==  True:
            return value
            break
        print(shown_error)

def verifOPlist(list,shown,shown_error='',showop_error='\033[1;31mOpção inválida! \033[1;33mEscolha outra opção.'):
    from Core.modulos.verif import verifname
    while True:
        value = verifname(shown,shown_error)
        if value in list:
            return value
            break
        print(showop_error)

def verifOPdict(dict,shown,showop_error='\033[1;31mOpção inválida! \033[1;33mEscolha outra opção.'):
    from Core.modulos.verif import verifname
    while True:
        value = verifname(shown)
        for pos, extract1 in dict:
            for extract2 in extract1.key:
                if value == extract2:
                    return value
                    aunt = True
                    break
                else:
                    aunt = False
        if aunt == False:
            print(showop_error)




