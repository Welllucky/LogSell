import Core.modulos.verif
import Core.modulos.show
from operator import itemgetter
from time import sleep as s
Cad_list= [{'rn7':1300},{'nitro':4000},{'miband4':300},{'airdots':200},{'0':0}]
options = {'listalt': ['produtos','preços','s'],'listopção':['nome','id']}
aunt = False

print('\033[1;30m-'*55)
print(f'\033[1;33mLog\033[1;32mSell\033[m'.center(68))
print('\033[1;30m-\033[m'*55)
s(2)
while True:
    print('\033[1;32m{:^50}'.format('PRODUTOS'))
    s(0.7)
    print(f'\033[1;33m1 \033[1;30m- \033[1;32mMostrar em ordem alfabética os nomes do produtos\n'
                  f'\033[1;33m2 \033[1;30m- \033[1;32mLocalizar um produto\n'
                  f'\033[1;33m3 \033[1;30m- \033[1;32mRemove um produto\n'
                  f'\033[1;33m4 \033[1;30m- \033[1;32mAdicionar um produto\n'
                  f'\033[1;33m5 \033[1;30m- \033[1;32mMostrar preços dos produtos\n'
                  f'\033[1;33m6 \033[1;30m- \033[1;32mMostra posição dos produtos\n'
                  #f'8 - Mostrar informações do produtos'
                  f'\033[1;33m7 \033[1;30m- \033[1;32mEncerrar o programa!\033[m')

    alt2 = Core.modulos.verif.verifnum('\n\033[1;33mEscolha uma das alternativas:\033[m ')
    print(alt2), s(2)
    while True:

        if alt2 == 1: #Mostrar em ordem alfabética os nomes do produtos
            print(' ')
            print('\033[1;31mLista de Produtos\033[m')
            temp = list()
            for produtos in Cad_list:
                for use in produtos:
                    temp.append(use)

            print(f'{Core.modulos.show.acent(sorted(temp))}')
        s(3)
        break

        if alt2 == 2:
            print('\033[1;31mLocalizar Produto\033[m')
            print('De qual forma deseja localizar o produto?')
            opção = Core.modulos.verif.verifOPlist(options['listopção'], 'Nome/ID: ')

            if opção == options['listopção'][0]:
                while True:
                    nome_produto = Core.modulos.verif.verifname('\033[1;32mDigite o nome do produto desejado: ')
                    if nome_produto == 's':
                        break
                    for produtos in Cad_list:
                        for pos, prod in enumerate(produtos.keys()):
                            if nome_produto == prod:
                                print(
                                    f'O produto "{nome_produto}" está localizado na {pos + 1}° posição da lista de cadastro')
                                s(3)
                                aunt = True
                            else:
                                aunt = False

                    if aunt == False:
                        print('\033[1;31mO produto não foi cadastrado\033[m')
                        s(2)
                    alt3 = Core.modulos.verif.verifname('\033[1;33mDeseja localizar algum outro produto: ',
                                                   '\033[1;31mAltenativa incorreta, digite [S/N]')
                    if alt3 == 'não':
                        break
                    print('\n')
                break
            if opção == options['listopção'][1]:
                while True:
                    print(f'\nHá {len(Cad_list)} IDs disponíveis para consulta')
                    id_produto = Core.modulos.verif.verifnum('\033[1;32mDigite o ID do produto: ',
                                                        '\033[1;31mCaractere inválido') - 1
                    if id_produto == 0:
                        break
                    for pos, produtos in enumerate(Cad_list):
                        if id_produto == pos:
                            for prod in produtos.keys():
                                print(
                                    f'O produto pertencente ao ID \033[1;31m{id_produto + 1}\033[0;30m é o \033[1;32m"{prod}"\033[0m')
                    if id_produto > len(Cad_list):
                        print('\033[1;31mID inválido\033[m')
                    alt3 = Core.modulos.verif.verifname('\033[1;33mDeseja consultar outro ID?: ',
                                                   '\033[1;31mAltenativa incorreta, digite [S/N]')[0]
                    if alt3 == 'n':
                        break
                    print('\n')
                s(1)
                break

        if alt2 == 3:
            print(' ')
            print('\033[1;31mRemover Produto\033[m')
            print('De qual forma deseja remover o produto?')
            opção = Core.modulos.verif.verifOPlist(options['listopção'], 'Nome/ID: ')

            if opção == options['listopção'][0] :
                while True :
                    nome_produto = Core.modulos.verif.verifname('Digite o nome do produto desejado: ')
                    if nome_produto == 's' :
                        break
                    while aunt != True :
                        for pos, produtos in enumerate(Cad_list) :
                            for prod in produtos.keys() :
                                if nome_produto == prod :
                                    print('Correto')
                                    Cad_list.remove(Cad_list[pos])
                                    aunt = True
                        break
                    if aunt == False :
                        print(f'\033[1;31mO item "{nome_produto}" não está na lista.\033[m')
                        s(2)
                    # modulos.show.acent()
                    alt3 = modulos.verif.verifname('Deseja deletar outro elemento da lista? [S/N]:',
                                                           'Opção inválida, digite [S/N]')
                    if alt3 == 'n' :
                        break
                    print('\n')
            if opção == options['listopção'][1] :
                while True :
                    print(f'Há {len(Cad_list)} IDs disponíveis para consulta')
                    id_produto = Core.modulos.verif.verifnum('Digite o ID do produto: ') - 1
                    if id_produto == 0 :
                        break
                    Cad_list.remove(Cad_list[id_produto])
                    alt3 = Core.modulos.verif.verifname('Deseja deletar outro elemento da lista? [S/N]:',
                                                           'Opção inválida, digite [S/N]')
                    if alt3 == 'n' :
                        break
                    print('\n')
            s(3)
            break

        if alt2 == 4:
            print(' ')
            print('\033[1;31mAdicionar Produto\033[m')
            while True:
                Cad_list.append({Core.modulos.verif.verifname('Digite o nome do produto: '): Core.modulos.verif.verifnum('Digite o valor do produto: ')})
                print(f'{Cad_list[-1]} foi adicionado com sucesso!')
                alt3 = Core.modulos.verif.verifname('Deseja adicionar mais algum produto?[S/N]: ','Opção inválida, digite [S/N]')
                if alt3 == 'n':
                    break
            s(2)
            break

        if alt2 == 5:
            print(' ')
            print('\033[1;31mLista de Produtos\033[m')
            for itens in Cad_list:
                for prod in itens.items():
                    print(f'\033[1;32m{prod[0]}\033[1;33m..................................................\033[1;32mR${prod[1]}')
            s(2)
            break

        if alt2 == 6:
            print(' ')
            print('\033[1;31mLocalizar Produto\033[m')
            for pos, itens in enumerate(Cad_list):
                for prod in itens.keys():
                    print(f'"{prod}" está na {pos+1}° posição de cadastro')
            s(3)
            break

        if alt2 == 7:
            print('\n\033[1;31mEncerrando Programa!')
            s(0.5)
            for pos in range(0,4,-1):
                print(pos,end=' ')
                s(1)
                print('. ',end=' ')
            quit()