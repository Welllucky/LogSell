import Core.modulos.verif
import Core.modulos.show
from operator import itemgetter
from time import sleep as s
#products = ['avião','airdots0','rn7']
#prices = [100,4324,503,12,642]

#name = modulos.verif.verifname()
Cad_list = [{'rn7':1300},{'nitro':4000},{'miband4':300},{'airdots':200},{'0':0}]
options = {'listalt': ['produtos','preços','s'],'listopção':['nome','id']}
aunt = ''
print(' ')
print('\033[1;31mLocalizar Produto\033[m')
print('De qual forma deseja localizar o produto?')
opção = Core.modulos.verif.verifOPlist(options['listopção'], 'Nome/ID: ')
print(opção), s(2)
while True:
    if opção == options['listopção'][0] :
        nome_produto = Core.modulos.verif.verifname('\033[1;32mDigite o nome do produto desejado: ')
        print(nome_produto), s(2)
        while True:
            if nome_produto == 's' :
                break
            for produtos in Cad_list :
                for pos, prod in enumerate(produtos.keys()) :
                    print('\033[1;31m',prod), s(1)
                    if nome_produto == prod :
                        print(f'O produto "{prod}" está localizado na {pos + 1}° posição da lista de cadastro')
                        aunt = True
                        break
                        s(3)
                    else:
                        aunt = False
                    print('\033[1;32m', aunt), s(2)
            if aunt == False :
                print('\033[1;31mO produto não foi cadastrado\033[m')
                s(2)
            alt3 = Core.modulos.verif.verifname('\033[1;33mDeseja localizar algum outro produto: ',
                                                    '\033[1;31mAltenativa incorreta, digite [S/N]')
            if alt3 == 'não' :
                break
            print('\n')
    if opção == options['listopção'][1] :
        while True :
            print(f'\nHá {len(Cad_list)} IDs disponíveis para consulta')
            id_produto = Core.modulos.verif.verifnum('\033[1;32mDigite o ID do produto: ',
                                                         '\033[1;31mCaractere inválido') - 1
            print(id_produto), s(2)
            if id_produto == 0 :
                break
            for pos, produtos in enumerate(Cad_list) :
                if id_produto == pos :
                    for prod in produtos.keys() :
                        print(f'O produto pertencente ao ID \033[1;31m{id_produto + 1}\033[0;30m é o \033[1;32m"{prod}"\033[0m')
            if id_produto > len(Cad_list) :
                print('\033[1;31mID inválido\033[m')
            alt3 = Core.modulos.verif.verifname('\033[1;33mDeseja consultar outro ID?: ',
                                                    '\033[1;31mAltenativa incorreta, digite [S/N]')[0]
            if alt3 == 'n' :
                break
            print('\n')
        s(1)
